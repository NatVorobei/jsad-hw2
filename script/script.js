// Конструкцію try..catch можна використовувати:
// при обробці даних, наприклад коли користувач в полі вводу ввів невірний формат даних, ми можемо сповістити його за допомогою сформованої помилки.
// при обробці масивів та списків
// при роботі за базою даних

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

let ul = document.createElement('ul');
let div = document.getElementById('root');

function hasAllRequiredProperties(book) {
  const requiredProperties = ['author', 'name', 'price'];

  for (let i = 0; i < requiredProperties.length; i++) {
  	if (!book.hasOwnProperty(requiredProperties[i])) {
      throw new Error(`Неповні дані. Відсутня властивість ${requiredProperties[i]}.`);
    }
  }
}

function createListItem(book) {
  const li = document.createElement('li');
  li.textContent = `
    Автор: ${book.author},
    Назва: ${book.name},
    Ціна: ${book.price}
  `;
  return li;
}

books.forEach((book) => {
  try {
    hasAllRequiredProperties(book);
    const li = createListItem(book);
    ul.appendChild(li);
    div.appendChild(ul);
  } catch (err) {
    console.error(err.message);
  }
});
  
  

